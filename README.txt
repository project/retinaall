
RETINA ALL
==========

DESCRIPTION
-----------

Causes all image styles to use high-resolution images.

If you want to use retina images for your site, you probably want to use it for
all images.  You also probably want to use a method that requires minimal
configuration and ongoing maintenance.  This module does that.

No need to add and configure image effects for each image style.

TECHNIQUE
---------

Uses the Thomas Fuchs method:
- creating images at double height and width
- use height and width attributes on the img tags to display them at the
original size
- decrease the JPEG quality to prevent huge files that show detail which cannot
be seen by the human eye.

For more information see:
https://mir.aculo.us/2012/08/06/high-resolution-images-and-file-size/
http://www.netvlies.nl/blog/design-interactie/retina-revolution

INSTALLATION
------------

1. Unpack the files just like any other module.  Probably at something like:
    sites/all/modules/contrib/retinaall
2. Enable the Retina All module (Other category) at:
    admin/modules
3. Change the dimensions of all image styles to use twice the width and height.
   Allow upscaling.
    admin/config/media/image-styles
4. Optional: Make minor tweaks to the JPEG quality:
    admin/config/media/image-toolkit

DEVELOPED BY
------------
Dave Hansen-Lange
David Rothstein

SPONSORED BY
------------

Advomatic
http://advomatic.com

Threespot
https://www.threespot.com/

The Clinton Foundation
http://www.clintonfoundation.org/
